package main

import (
	"database/sql"
	"fmt"
	"net/http"
	"text/template"

	"github.com/codegangsta/negroni"
	_ "github.com/go-sql-driver/mysql"
)

var db *sql.DB
var err error
var templates *template.Template

func main() {

	//database
	db, err = sql.Open("mysql", "albert:pass123@tcp(127.0.0.1:3306)/requestDB?parseTime=true")
	if err != nil {
		fmt.Println(err)
	}
	defer db.Close()
	//router
	router := NewRouter()
	//negroni middleware
	n := negroni.Classic()
	//static files
	static := negroni.NewStatic(http.Dir("static/"))
	static.Prefix = "/static"
	n.Use(static)
	//static files
	libs := negroni.NewStatic(http.Dir("../libs/"))
	libs.Prefix = "/libs"
	n.Use(libs)
	n.UseHandler(router)
	n.Run(":8081")
}
