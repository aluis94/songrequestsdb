package main

import (
	"net/http"
)

//Route struct
type Route struct {
	Name        string
	Method      string
	Pattern     string
	HandlerFunc http.HandlerFunc
}

//Routes slice
type Routes []Route

var routes = Routes{
	Route{
		"Index",
		"GET",
		"/",
		Index,
	},
	Route{
		"RequestIndex",
		"GET",
		"/request",
		RequestIndex,
	},
	Route{
		"RequestShow",
		"GET",
		"/request/{requestID}",
		RequestShow,
	},
	Route{
		"RequestCreate",
		"POST",
		"/request",
		RequestCreate,
	},
	Route{
		"RequestUpdate",
		"POST",
		"/request/update",
		RequestUpdate,
	},
	Route{
		"NewRequest",
		"GET",
		"/new-request",
		RequestNew,
	},
}
