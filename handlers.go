package main

import (
	"encoding/json"

	"github.com/yosssi/ace"

	"net/http"
	"strconv"

	"github.com/gorilla/mux"
)

//Index handle func
func Index(w http.ResponseWriter, r *http.Request) {
	var context map[string]string
	context = map[string]string{"filter": "none"}
	if filter := r.FormValue("filter"); filter != "" && filter == "today" {
		context = map[string]string{"filter": filter}
	}

	//ace templates
	template, err := ace.Load("templates/base", "templates/index", nil)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
	}

	if err = template.Execute(w, context); err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
	}

}

//RequestIndex handle func
func RequestIndex(w http.ResponseWriter, r *http.Request) {
	var requests Requests
	requests = getRequests("select * from request;")
	if filter := r.FormValue("filter"); filter == "today" {
		requests = getRequests("select * from request where timestamp >= curdate() && timestamp < (curdate() + interval 1 day) ;")
	}

	//requests := getRequests("select * from request where timestamp >curdate() and timestamp < curdate()+interval 1 DAY;") //get all requests for today
	w.Header().Set("Content-Type", "application/json; charset=UTF-8")
	w.WriteHeader(http.StatusOK)
	if err := json.NewEncoder(w).Encode(requests); err != nil {
		panic(err)
	}
}

//RequestShow handle func
func RequestShow(w http.ResponseWriter, r *http.Request) {
	vars := mux.Vars(r)
	requestID := vars["requestID"]
	intrequestID, _ := strconv.Atoi(requestID)
	req := getSingleRequest("select * from request where id=?;", intrequestID)
	context := map[string]string{"ID": req.ID, "Name": req.Name, "Artist": req.Artist, "Status": req.Status, "Formtype": "Edit"}
	template, err := ace.Load("templates/base", "templates/request", nil)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
	}
	if err = template.Execute(w, context); err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
	}
}

//RequestNew handler
func RequestNew(w http.ResponseWriter, r *http.Request) {
	//ace templates
	template, err := ace.Load("templates/base", "templates/request", nil)
	context := map[string]string{"ID": "", "Name": "", "Artist": "", "Status": "", "Formtype": "New"}
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
	}
	if err = template.Execute(w, context); err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
	}
}

//RequestCreate handle func //REST endpoints
func RequestCreate(w http.ResponseWriter, r *http.Request) {
	request := getJSONBodyData(w, r)
	req := createRequest(request)
	w.Header().Set("Content-Type", "application/json; charset=UTF-8")
	w.WriteHeader(http.StatusCreated)
	if err := json.NewEncoder(w).Encode(req); err != nil {
		panic(err)
	}
}

//RequestUpdate handle func
func RequestUpdate(w http.ResponseWriter, r *http.Request) {
	request := getJSONBodyData(w, r)
	req := updateRequest(request)
	w.Header().Set("Content-Type", "application/json; charset=UTF-8")
	w.WriteHeader(http.StatusCreated)
	if err := json.NewEncoder(w).Encode(req); err != nil {
		panic(err)
	}
}
