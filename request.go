package main

import "time"

//Request struct
type Request struct {
	ID        string    `json:"ID"`
	Name      string    `json:"name"`
	Artist    string    `json:"artist"`
	Status    string    `json:"status"`
	TimeStamp time.Time `json:"timestamp"`
}

//Requests slice
type Requests []Request
