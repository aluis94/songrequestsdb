package main

import (
	"database/sql"
	"encoding/json"
	"fmt"
	"io"
	"io/ioutil"
	"log"
	"net/http"
	"strconv"
	"time"
)

func getRequests(sqlString string) Requests {
	id := ""
	name := ""
	artist := ""
	status := ""
	var timeStamp time.Time
	requests := Requests{}
	rows, err := db.Query(sqlString)
	if err != nil {
		log.Fatal(err)
	}
	defer rows.Close()
	for rows.Next() {
		err := rows.Scan(&id, &name, &artist, &status, &timeStamp)
		if err != nil {
			log.Fatal(err)
		}
		request := Request{ID: id, Name: name, Artist: artist, Status: status, TimeStamp: timeStamp}
		requests = append(requests, request)
	}
	return requests
}

func getSingleRequest(sqlStatement string, id int) Request {

	name := ""
	artist := ""
	status := ""
	var req Request
	var timeStamp time.Time
	row := db.QueryRow(sqlStatement, id)
	switch err := row.Scan(&id, &name, &artist, &status, &timeStamp); err {
	case sql.ErrNoRows:
		fmt.Println("No rows were returned!")
	case nil:
		req = Request{ID: strconv.Itoa(id), Name: name, Artist: artist, Status: status}
	default:
		panic(err)
	}
	return req
}

func createRequest(request Request) Request {
	status := "queued"
	fmt.Println(request.Name, request.Artist, status)
	stmtIns, err := db.Prepare("INSERT INTO request(name, artist, status) VALUES( ?, ?, ?)") // ? = placeholder
	if err != nil {
		panic(err.Error()) // proper error handling instead of panic in your app
	}
	defer stmtIns.Close()
	_, err = stmtIns.Exec(request.Name, request.Artist, status) // Insert tuples (i, i^2)
	return Request{}
}

func updateRequest(request Request) Request {
	id := request.ID
	name := request.Name
	artist := request.Artist
	status := request.Status

	fmt.Println(request.Name, request.Artist, status)
	stmtIns, err := db.Prepare("Update request set name=?, artist=?, status=? where id = ?") // ? = placeholder
	if err != nil {
		panic(err.Error()) // proper error handling instead of panic in your app
	}
	defer stmtIns.Close()
	_, err = stmtIns.Exec(name, artist, status, id) // update status
	return Request{}
}

func getJSONBodyData(w http.ResponseWriter, r *http.Request) Request {
	var request Request
	body, err := ioutil.ReadAll(io.LimitReader(r.Body, 1048576))
	if err != nil {
		panic(err)
	}
	if err := r.Body.Close(); err != nil {
		panic(err)
	}
	if err := json.Unmarshal(body, &request); err != nil {
		w.Header().Set("Content-Type", "application/json; charset=UTF-8")
		w.WriteHeader(422) //unprocessable entity
		if err := json.NewEncoder(w).Encode(err); err != nil {
			panic(err)
		}
	}
	return request
}
